use std::io::{BufRead, BufReader};
use std::io::Read;
use super::error::Error;

pub fn read_line<R: BufRead>(reader: R) -> Result<String, Error> {
    let mut line = String::new();
    let mut buf = BufReader::new(reader);
    if let Err(e) = buf.read_line(&mut line) {
        return Err(Error::IO(e));
    }
    Ok(line.trim_end_matches("\n").to_string())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::{Seek, SeekFrom, Write};

    #[test]
    fn read_line_can_read_from_arbitrary_cursor() {
        // Create fake "file"
        let mut c = std::io::Cursor::new(Vec::new());
        let expected_lines = [
            "hello this is",
            "two test lines",
        ];
        c.write(format!("{}\n{}", expected_lines[0], expected_lines[1]).as_bytes()).unwrap();
        c.seek(SeekFrom::Start(0)).unwrap();
        match read_line(c) {
            Ok(l) => {
                println!("Read '{}'", l);
                assert_eq!(l, expected_lines[0]);
            },
            Err(e) => {
                panic!(e);
            }
        }
    }
}