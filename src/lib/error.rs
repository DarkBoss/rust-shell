use std::io::Error as IOError;
use std::fmt::{Display, Formatter};
use std::fmt::Result as FmtResult;

#[derive(Debug)]
pub enum Error {
    Unknown{message: Option<String>},
    IO(IOError)
}

impl std::convert::Into<String> for Error {
    fn into(self) -> String {
        match self {
            Error::Unknown{message} => message.unwrap_or(String::new()).clone(),
            Error::IO(e) => e.to_string(),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{}", self.to_string())
    }
}